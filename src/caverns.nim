import sequtils
import random

randomize()

type
  MapTile* {.pure.} = enum
    Floor = 0,
    Wall = 1

type
  Cavern* = ref object of RootObj
    map: seq[seq[MapTile]]
    birthLimit: int
    deathLimit: int

proc width*(cavern: Cavern): int =
  cavern.map[0].len

proc height*(cavern: Cavern): int =
  cavern.map.len

proc wallCountMoore(cavern: Cavern, row, col, distance: int): int =
    var walls = 0
    for rIndex in -distance .. distance:
        for cIndex in -distance .. distance:
            var currentRow = row + rIndex
            var currentCol = col + cIndex
            if currentRow < 0 or currentRow >= cavern.height or currentCol < 0 or currentCol >= cavern.width or cavern.map[currentRow][currentCol] == MapTile.Wall:
                if not (rIndex == 0 and cIndex == 0):
                    walls += 1
    walls

proc smooth*(cavern: Cavern) =
    var map = cavern.map
    var newMap = newSeqWith(map.len, newSeq[MapTile](map[0].len))
    for row in 0 ..< cavern.height:
        for col in 0 ..< cavern.width:
            newMap[row][col] = if map[row][col] == MapTile.Wall and cavern.wallCountMoore(row, col, 1) == 3: MapTile.Floor else: map[row][col]
    cavern.map = newMap


proc subdivide*(cavern: Cavern) =
    var map = cavern.map
    var matrix = newSeqWith(map.len * 2, newSeq[MapTile](map[0].len * 2))
    for row in 0 ..< cavern.height:
        for col in 0 ..< cavern.width:
            matrix[row * 2][col * 2] = map[row][col]
            matrix[row * 2][col * 2 + 1] = map[row][col]
            matrix[row * 2 + 1][col * 2] = map[row][col]
            matrix[row * 2 + 1][col * 2 + 1] = map[row][col]
    cavern.map = matrix
    cavern.smooth

proc `$`*(cavern: Cavern): string =
    result = ""
    let map = cavern.map
    for row in 0 ..< cavern.height:
        for col in 0 ..< cavern.width:
            result &= (if map[row][col] == MapTile.Wall: "#" else: ".")
        result &= "\n"

proc shouldBeWall(cavern: Cavern, row, col: int): bool =
  let map = cavern.map
  var walls = 0
  for rowIndex in (row - 1)..(row + 1):
    for colIndex in (col - 1)..(col + 1):
      if not (rowIndex == row and colIndex == col) and (rowIndex < 0 or rowIndex >= cavern.height or colIndex < 0 or colIndex >= cavern.width or map[rowIndex][colIndex] == MapTile.Wall):
        walls += 1
  if map[row][col] == MapTile.Wall: walls >= cavern.deathLimit else: walls > cavern.birthLimit

proc nextGeneration(cave: Cavern): Cavern =
  var newMap = newSeqWith(cave.map.len, newSeq[MapTile](cave.map[0].len))
  for row in 0 ..< cave.height:
      for col in 0 ..< cave.width:
          newMap[row][col] = if cave.shouldBeWall(row, col): MapTile.Wall else: MapTile.Floor
  cave.map = newMap
  cave

proc initial(width, height, birthLimit, deathLimit: int): Cavern =
  var matrix = newSeqWith(height, newSeq[MapTile](width))
  for row in 0 .. <matrix.len:
    for col in 0 .. <matrix[row].len:
      matrix[row][col] = if random(100) < 40: MapTile.Wall else: MapTile.Floor
  Cavern(map: matrix, birthLimit: birthLimit, deathLimit: deathLimit)

proc createCavern*(width, height, iterations, birthLimit, deathLimit: int): Cavern =
    result = initial(width, height, birthLimit, deathLimit)
    for i in 0 ..< iterations:
        result = nextGeneration(result)

# var cavern = createCavern(width = 48, height = 48, iterations = 6, birthLimit = 4, deathLimit = 3)
# cavern.subdivide
# echo $cavern
