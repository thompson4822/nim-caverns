import einheit, caverns

testSuite CavernTests:
  var
    cavern: Cavern
    # testArray: array[4, int]

  # proc doThings() =
  #   # This proc won't be invoked as a test
  #   self.testObj = 400
  #   self.check(self.testObj == 400)

  # method setup() =
  #   self.testObj = 90
  #   for i in 0 ..< self.testArray.len():
  #     self.testArray[i] = i

  # method tearDown() =
  #   self.testObj = 0

  method testCreate() =
    self.cavern = createCavern(width = 42, height = 38, iterations = 1, birthLimit = 3, deathLimit = 4)
    self.check(self.cavern.width == 42)
    self.check(self.cavern.height == 38)

  # method testForB() =
  #   var b = 4
  #   self.doThings()
  #   self.check(b == 4)
  #
  # method testArrayAssert() =
  #   self.check(self.testArray == [0,1,2])
  #
  # method testForC() =
  #   var c = 0
  #   # supposed to fail
  #   self.check(c == 1)

when isMainModule:
  runTests()
